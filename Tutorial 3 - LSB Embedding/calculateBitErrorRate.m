function [ber] = calculateBitErrorRate(originalImage, testImage)
    if numel(originalImage) ~= numel(testImage)
        error ('Images are of a different size');
    else
        originalImage = double(originalImage);
        testImage = double(testImage);

        % Subtracts the LSB of each pixel within each of the images
        % If the LSB of any pixel differs it will now have a non-zero element
        % Counts the number of non-zero elements to retrieve the number of instances were the LSB differs
        errors = nnz(bitget(testImage,1)-bitget(originalImage,1));

        % Calculate the BER from the number of bit errors
        % And number of pixels within the image
        ber = errors/numel(originalImage);
    end
end
