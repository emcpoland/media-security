function [embeddedImage] = embedLSBWatermark(image, watermark)
    if size(watermark) > size(image)
        error ('Watermark is too large for the image');
    else
        [iWidth, iHeight] = size(image);
        % Flaten 2D -> 1D
        image = reshape(image, iWidth*iHeight, 1);
        % Set the last bit of each pixel within the image to that of the watermark
        embeddedImage = bitset(image, 1, watermark);
        % Reshape 1D -> 2D with original dimensions
        embeddedImage = reshape(embeddedImage, iWidth, iHeight);
    end
end
