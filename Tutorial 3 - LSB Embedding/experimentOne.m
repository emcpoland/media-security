function [bwImage, watermark, embeddedImage] = experimentOne(imageFileName, OUTPUT_FOLDER)
	% Load image file and convert to greyscale if required
	% All following operations within this practical relies on using greyscale image
	originalImage = imread(imageFileName);
	if(size(originalImage, 3) > 1)
		bwImage = rgb2gray(originalImage);
	else
		bwImage = originalImage;
	end

	% Generate a random watermark, with the original filename as the seed
	% The seed value always us to generate the same watermark for the image each time
	% Which is useful when attempting to analyse the images
	% The watermak should have the same length  as the number of pixels in the image
	% Save the watermark result
	watermark = generateRandom1DWatermark(numel(bwImage), sum(uint8(imageFileName)));
	%imwrite(watermark, [OUTPUT_FOLDER, 'watermark.bmp']);


	% Embed the watermark into the image
	embeddedImage = embedLSBWatermark(bwImage, watermark);
	imwrite(embeddedImage, [OUTPUT_FOLDER, 'embeddedImage.bmp']);

	% Find the difference between the watermark image and the original image
	% ie It shows the watermark that was embedded
	differenceImage = abs( double(bwImage) - double(embeddedImage) );
	figure, imshow(uint8(120 * differenceImage)), title('Difference Image');

	% Save the greyscale and difference image
	% It wasnt requested for this experiment but its useful for the report
	imwrite(bwImage, [OUTPUT_FOLDER, 'bwImage.bmp']);
	imwrite(differenceImage, [OUTPUT_FOLDER, 'differenceImage.bmp']);
end
