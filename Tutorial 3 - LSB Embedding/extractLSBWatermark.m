function [watermark] = extractLSBWatermark(image, wSize)
    % Get the LSB of each pixel
	watermark = bitget(image, 1);
    % Flatten 2D -> 1D
    watermark = reshape(watermark, numel(image), 1);
    % Remove the trailing bits beyound the watermark length
    watermark = watermark(1:wSize);
end
