function [extractedWatermarkBER] = experimentThree(embeddedImage, watermark)
	% Extract the watermark from the image
	extractedWatermark = extractLSBWatermark(embeddedImage, numel(watermark));

	% Save the extracted watermark so we can compare it to the original via MD5 hash
	%imwrite(extractedBaboonWatermark, [OUTPUT_FOLDER, '/extractedWatermark.bmp'])

	% Calculate the BER of the original watermark to the extracted watermark
	extractedWatermarkBER = calculateBitErrorRate(watermark, extractedWatermark);

	% Octave doesnt support table (ie Tabulating data)
	% So will do this in the final report as opposed to using MatLab for it
end
