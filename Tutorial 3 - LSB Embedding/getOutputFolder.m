% Creates output folder depending on image file name
function [outputFolder] = getOutputFolder(imageFileName)
	[pathStr, name, ext] = fileparts(imageFileName);
	outputFolder = strcat('./output/', name, '/');
	mkdir(outputFolder);
end
