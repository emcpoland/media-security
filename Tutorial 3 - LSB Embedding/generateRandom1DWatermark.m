function [watermark] = generateRandom1DWatermark(mSize, seed)
    % Save current seed value so we can it later
    % Essentially scope the seed value
    % Sets the new seed value
    % Which allows us to consistently generate the same 'random' watermark
    original_seed = rand('seed');
    rand('seed', seed);

    % Generate random 1D array with a value between 0<->1 for each pixel in the image
    message = rand(mSize, 1);

    % Converts random decimal value to binary, 0 or 1
    x0 = find (message <= 0.5);
    x1 = find (message > 0.5);
    message(x0) = 0;
    message(x1) = 1;

    watermark = message;

    % Reset the seed value
    rand('seed', original_seed);
end
