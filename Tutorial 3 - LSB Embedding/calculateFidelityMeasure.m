function [PSNR] = calculateFidelityMeasure(originalImage, testImage)
	originalImage = double(originalImage);
	testImage = double(testImage);

	N = numel(originalImage);
	MSE = sum(sum((originalImage-testImage).^2)) / N;

	PSNR = 10*log10((255^2)/MSE);
end
