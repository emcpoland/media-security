clear; % Clear workspace
close all; % Close previously opened figures
mkdir('output'); % Setup directory structure
%pkg load image % Import octave image package

% The script was originally written for octave,
% Some minor modifications were required to get it to work for MatLab
% Also noticed the results were ever so slightly different
% The values in the report uses the Matlab results.

% Run each experiment for multiple images
for imageFileName = {'input/baboon256.bmp', 'input/polarbear512.bmp', 'input/rice.png'}
	fprintf(['Running experiments for ', imageFileName{1}, '\n\n'])

	%Setup the output folder
	OUTPUT_FOLDER = getOutputFolder(imageFileName{1});

	% Run each of the experiments
	% The experiments rely on the use of a greyscale subject image
	% On several of the function we were requested to use for-loops
	% However this was shown to be incredibly slow and unesscary, expecially in octave
	% Hence I removed the for-loop methods and replaced it with something considerably faster
	% However all of the results were compared against the original for-loop methods and they produced the same result

	[bwImage, watermark, embeddedImage] = experimentOne(imageFileName{1}, OUTPUT_FOLDER);
	embeddedPSNR = experimentTwo(bwImage, embeddedImage)

	extractedWatermarkBER = experimentThree(embeddedImage, watermark)

	[filteredEmbeddedPSNR, attackedExtractedWatermarkBER] = experimentFour(embeddedImage, OUTPUT_FOLDER)
	fprintf('\n\n\n')

end
