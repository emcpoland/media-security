function [filteredEmbeddedPSNR, attackedExtractedWatermarkBER] = experimentFour(embeddedImage, OUTPUT_FOLDER)
	% Apply Gaussian Filter and save the result
	gaussianFilter = fspecial('gaussian', [3,3], 1.5);
	attackedEmbeddedImage = imfilter(embeddedImage, gaussianFilter);
	imwrite(attackedEmbeddedImage, [OUTPUT_FOLDER, 'attackedEmbeddedImage.bmp'])

	% Calculate the PSNR and BER of the difference between the original embedded image and the now degraded embedded image
	filteredEmbeddedPSNR = calculateFidelityMeasure(embeddedImage, attackedEmbeddedImage);
	attackedExtractedWatermarkBER = calculateBitErrorRate(embeddedImage, attackedEmbeddedImage);
end
