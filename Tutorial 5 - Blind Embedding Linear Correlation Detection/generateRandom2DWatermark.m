function [watermark] = generateRandom2DWatermark(width, height, seed)
    % Save current seed value so we can reset it later
    % Essentially scope the seed value
    % Sets the new seed value
    % Which allows us to consistently generate the same 'random' watermark
    % Should make this an optional input argument
    original_seed = randn('seed');
    randn('seed', seed);

    % Generate random 2D array with a value between 0<->1
    watermark = randn(width, height);

    % Set N(0,1)
    watermark = watermark-mean2(watermark);
    watermark = watermark/std2(watermark);

    % Reset the seed value
    randn('seed', original_seed);
end
