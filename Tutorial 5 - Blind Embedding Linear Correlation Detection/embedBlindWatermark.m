function [embeddedImage] = embedBlindWatermark(image, watermark, messageBit, alphaValue)
    image = double(image);

    %Alter watermark depending on message bit
    if messageBit == 0
        messagePattern = - watermark;
    else
        messagePattern = watermark;
    end

    %Embed altered watermark into image
    embeddedImage = image+alphaValue*messagePattern;
    embeddedImage = uint8(embeddedImage);
end
