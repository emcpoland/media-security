function [linearCorrelation] = calculateLinearCorrelation(originalSubject, testSubject)
    originalSubject = double(originalSubject);
    testSubject = double(testSubject);

    %Multiply each element in originalSubject with its corresponding element in testSubject
    %Sum the results of all these multiplcations (Row and column summation)
    %Divide by the number of pixels in the subject, to obtain an average value
	[width, height] = size(originalSubject);
    linearCorrelation = sum(sum(originalSubject.*testSubject))/(width*height);
end
