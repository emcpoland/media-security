function [cac, ca0, ca1] = test
    maxNumberOfImages = 800;

    %Efficency improvement
    cac = zeros(maxNumberOfImages, 1);
    ca0 = zeros(maxNumberOfImages, 1);
    ca1 = zeros(maxNumberOfImages, 1);

    for imageNumber = 1:maxNumberOfImages
        % Load image file
        imageFileName = strcat('input/', num2str(imageNumber), '.bmp');
        originalImage = imread(imageFileName);
        [width, height] = size(originalImage);

        % Generate a random watermark, with the image number as the seed
        % The seed value allows us to generate the same watermark for the image each time
        % Which is useful when attempting to analyse the images
        % The watermark should be of the same height and width as the image
    	watermark = generateRandom2DWatermark(width, height, imageNumber);

        % Embed the watermark into the image with the specified values of m=0 and m=1
        embeddedImageM0 = embedBlindWatermark(originalImage, watermark, 0, 1);
        embeddedImageM1 = embedBlindWatermark(originalImage, watermark, 1, 1);

        cac(imageNumber) = calculateLinearCorrelation(watermark, originalImage);
        ca0(imageNumber) = calculateLinearCorrelation(watermark, embeddedImageM0);
        ca1(imageNumber) = calculateLinearCorrelation(watermark, embeddedImageM1);
    end
end
