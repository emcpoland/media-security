clear; % Clear workspace
close all; % Close previously opened figures
mkdir('output'); % Setup directory structure
%pkg load image nan % Import octave packages

[cac, ca0, ca1] = test();

plotCorrelationArrays(cac, ca0, ca1);

%Calculate the detection rates for different threshold values
for threshold = [0, 0.2, 0.5, 0.7, 1]
	[drc, dr0, dr1] = calculateDetectionRates(cac, ca0, ca1, threshold);

	fprintf('\n'), threshold, drc, dr0, dr1, fprintf('\n')
end
