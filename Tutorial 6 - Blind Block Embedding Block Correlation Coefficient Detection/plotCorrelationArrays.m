function plotCorrelationArrays(cac, ca0, ca1)
	[Nc, Xc] = hist(cac, 10);
	[N0, X0] = hist(ca0, 10);
	[N1, X1] = hist(ca1, 10);

	%Plot the three graphs on a single figure
	figure, plot(Xc, Nc, '-r', X0, N0, '--g', X1, N1, '-.b'),
	legend('Original Image    ','m = 0    ', 'm = 1    ', 'Location', 'northwest'),
	print -dtex './output/histogram.tex';
end
