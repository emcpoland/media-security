\documentclass{article}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{lscape}
\usepackage{hhline}
\usepackage{float}
\usepackage{multirow}

\setlength{\parindent}{0pt}
\setlength{\parskip}{1em}

\begin{document}

\title{Practical Class 6: Block Blind Embedding / Block Correlation Coefficient}
\author{Media Security CSC4066\\Dr Fatih Kurugollu\\\\Emmet McPoland - 40060420}
\date{14th November 2015\\}

\maketitle
\renewcommand{\abstractname}{}
\begin{abstract}
The objective of the following document is to draw conclusions from an investigation into the use of Cox et al.'s block blind embedding and block correlation coefficient detection. Included within the document are the results of the embedding process on 800 images with an alternating message bit, and the success in detecting the watermark against various threshold values. The source code used to carry out the process has also been included and may be found within Appendix \ref{app:scripts}.
\end{abstract}

\pagebreak
\section*{Correlation}
Three image sets were used during the correlation phase, they include the 800 images that were embedded with m=1, the same 800 images that were embedded with m=0, and the original 800 unembedded images. Figure \ref{fig:histogram} shows the distribution of the correlation coefficients between each image in the three sets and the original watermark pattern.

\begin{figure}[H]
    \centering
    \resizebox{\textwidth}{!}{\input{./output/histogram.tex}}
    \caption[] {\small The distribution of the correlation coefficients between the original watermark pattern and three separate sets of images.}
    \label{fig:histogram}
\end{figure}

As can be seen in Figure \ref{fig:histogram}, the images which belong to the set embedded with m=0, tend towards but do not exceed -1, while the images which belong to the set embedded with m=1 tend towards but do not exceed 1, and finally the images belonging to the original set are distributed around 0. Using the above property we are capable of determining if an image contains a watermark and if it does, it is possible to extract the message bit used during its embedding phase.

However while each image is distributed around the relevant points, few lie precisely on the points. Therefore it is required to dictate a threshold value, in which if an image lies within such a threshold it can be considered to belong to that set.
%
\section*{Threshold}
\begin{center}
    \def\arraystretch{2}
    \begin{table}[h]
        \centering
        \begin{tabular}{| c || c | c | c |} \hline
            &\multicolumn{3}{c|}{Detection Rate} \\
            Threshold & Unwatermarked & m=0 & m=1 \\ \hline
            0.0 & 0 & 1 & 1 \\ \hline
            0.2 & 0.88875 & 1 & 1 \\ \hline
            0.5 & 1 & 0.99875 & 1 \\ \hline
            0.7 & 1 & 0.97000 & 0.97375 \\ \hline
            1.0 & 1 & 0 & 0 \\ \hline
        \end{tabular}
        \caption{The detection rate of the watermark within each image set as the threshold is adjusted}
        \label{tbl:detectionRates}
    \end{table}
\end{center}
%
Table \ref{tbl:detectionRates} shows that in this instance a threshold value of 0.5 is close to optimal, as almost every image was successfully detected as either having no watermark or having been embedded with either m=0, or m=1. However unfortunately not every image with m=0 was successfully detected. An optimal value, if one exists is likely to be just below 0.5, as indicated by the decreasing detection rate of m=0, for an increasing threshold value.

 This is an unsurprising result taking into consideration Figure \ref{fig:histogram}, it is clear that each set can almost be successfully separated by placing a vertical line at $\pm0.5$. However it should be noted that the m=0 curve slightly exceeds the -0.5 division, which leads to the slightly less than optimal scenario discussed in the previous paragraph.

 Table \ref{tbl:detectionRates} further shows that as the threshold is increased, so to is the likelihood of false negatives in detecting a watermark, while a lower threshold increases the likelihood of false positives in the detection of a watermark. Once again referring to Figure \ref{fig:histogram}, this is an unsurprising result as when the threshold increases the dividing lines would tend away from 0, leaving more points to be grouped to having have a correlation of 0. Implying that there is no watermark, the same is true for the inverse. This is an important property to consider when devising a threshold value, as a particular system may prefer false positives over false negatives.

Other observations from Table \ref{tbl:detectionRates} include that between 0 and 0.2 there is a drastic reduction in false positives, while the reduction between 0.2 and 0.5 is not as drastic. The opposite is true for false negatives between 0.7 and 0.5 with no drastic increases, unlike between 0.7 and 1.0. This relationship is caused by the bell shaped curve which can be seen in Figure \ref{fig:histogram}. Finally a case can be made that images embedded with m=1 have a slightly improved detection rate over those embedded with m=0, which is the opposite result obtained in the previous practical which implemented Cox et al.'s non-block blind embedding. Another differentiation from the non-block blind embedding is the lack of correlation values exceeding $\pm1$, with all values falling between -1 and 1.


\section*{Conclusion}
In conclusion lower threshold values lead to a greater number of false positives, while higher threshold values lead to a greater number of false negatives. In this case the optimal threshold value lies just below 0.5. It was also discovered that the threshold has a non-linear relationship against the detection rate as demonstrated by the bell and half-bell shaped curves. Furthermore block-blind embedding results in all correlation values falling between -1 and 1, unlike non-block embedding which has values that exceed $\pm1$.

\section*{Code Walkthrough}

%How does the line below construct the watermarked image?
\begin{lstlisting}
wmIm(i:i+7,j:j+7) = orIm(i:i+7,j:j+7) + (vw - vo);
\end{lstlisting}
The embedding process consists of:
\begin{itemize}
\item Dividing the image into 8x8 blocks.
\item Obtaining the average 8x8 block.
\item Determining the message pattern to embed, from the message bit and watermark. Such that it tends around the average block values.
\item Embedding the message pattern, by subtracting the average value from it, such that it tends around zero, and embedding it into each of the 8x8 blocks of the original image. Essentially replacing the average 8x8 block with each 8x8 block present within the image.
\end{itemize}

The above line of code is used to implement the last point, by iterating over the image in steps of 8x8 and adding the message pattern block, minus the average block.\\\\

%How can this construct the extracted watermark?
\begin{lstlisting}
for i = 1:8:height
    for j = 1:8:width
        vo = vo + orIm(i:i+7,j:j+7);
    end
end
\end{lstlisting}

It obtains the average 8x8 block of the embedded image. Which equals (ignoring rounding, clipping errors etc) the original embedded message pattern as the same amount was added/subtracted to each 8x8 block during the embedding process.\\\\

% What does orIm(i:i+7,j:j+7) do?
\begin{lstlisting}
    orIm(i:i+7,j:j+7)
\end{lstlisting}
Extracts an 8x8 block of pixels from the image, starting at position (i,j).

\appendix
\pagebreak
\section{Results}
\subsection{Console}
\lstinputlisting{./output/console.txt}
\pagebreak
\section{Source Files}
\label{app:scripts}
\begin{landscape}

\pagebreak
\lstinputlisting{../main.m}

\pagebreak
\lstinputlisting{../test.m}

\pagebreak
\lstinputlisting{../embedBlockBlindWatermark.m}

\pagebreak
\lstinputlisting{../calculateBlockCorrelation.m}

\pagebreak
\lstinputlisting{../plotCorrelationArrays.m}

\pagebreak
\lstinputlisting{../generateRandom2DWatermark.m}

\pagebreak
\lstinputlisting{../calculateDetectionRates.m}

\end{landscape}

\end{document}
