function [embeddedImage] = embedBlockBlindWatermark(image, watermark, messageBit, alphaValue)
    % Convert the image into double format
    image = double(image);
    % Use size() function to obtain the image size
    [width, height] = size(image);

    % Determine the message pattern using the message bit and watermark pattern.
    if messageBit == 0
        messagePattern = - watermark;
    else
        messagePattern = watermark;
    end

    % Determine the number of blocks which are of size 8x8
    numberOfBlocks = (width/8)*(height/8);

    % Clear the extracted watermark array, vo
    vo = zeros(8,8);

    % Block wise scan the image and accumulate the pixel values in 8x8 blocks
    for i = 1:8:height
        for j = 1:8:width
            vo = vo + image(i:i+7,j:j+7);
        end
    end

    % Normalise vo with the number of blocks
    vo = vo / numberOfBlocks;

    %Calculate the adding mark, vw, using the vo, alpha value and the watermark pattern
    vw = vo + (alphaValue * messagePattern);

    %Initialise the watermark image, wmIm, using zeros() function with height and width of the original image
    embeddedImage = zeros(width, height);

    % Block wise scan the image and add the watermark in each block. Use the following code
    for i = 1:8:height
        for j = 1:8:width
            embeddedImage(i:i+7,j:j+7) = image(i:i+7,j:j+7) + vw - vo;
        end
    end

    % Convert the watermarked image to integer format.
    embeddedImage = uint8(embeddedImage);
end
