function [drc, dr0, dr1] = calculateDetectionRates(cac, ca0, ca1, threshold)
	%Count the number of elements between += threshold in cac (Centered around 0)
	%And divide by the number of elements there should be
	xc = find((cac <= threshold) & (cac >= -threshold));
	drc = numel(xc)/numel(cac);

	%Count the number of elements < -threshold in ca0 (Centered around -1)
	%And divide by the number of elements there should be
	x0 = find(ca0 < -threshold);
	dr0 = numel(x0)/numel(ca0);

	%Count the number of elements > threshold in ca1 (Centered around 1)
	%And divide by the number of elements there should be
	x1 = find(ca1 > threshold);
	dr1 = numel(x1)/numel(ca1);
end
