function [correlationCoefficent] = calculateBlockCorrelation(testSubject, originalSubject)
	% Convert the image into double format
    originalSubject = double(originalSubject);
    testSubject = double(testSubject);
    % Use size() function to obtain the image size
    [width, height] = size(originalSubject);

    % Determine the number of blocks which are of size 8x8
    numberOfBlocks = (width/8)*(height/8);

    % Clear the extracted watermark array, vo
    vo = zeros(8,8);

    % Block wise scan the image and accumulate the pixel values in 8x8 blocks
    for i = 1:8:height
        for j = 1:8:width
            vo = vo + originalSubject(i:i+7, j:j+7);
        end
    end

    % Normalise vo with the number of blocks
    vo = vo / numberOfBlocks;

	% Calculate correlation coefficient between vo and the watermark using corr2() function
	correlationCoefficent = corr2(vo, testSubject);
end
