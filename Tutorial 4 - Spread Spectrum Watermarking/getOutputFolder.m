% Creates output folder depending on name
function [outputFolder] = getOutputFolder(name)
	outputFolder = strcat('./output/', name, '/');
	mkdir(outputFolder);
end
