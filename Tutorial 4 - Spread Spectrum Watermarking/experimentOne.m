function [bwImage, watermark, embeddedImage] = experimentOne(imageFileName, alpha, position, OUTPUT_FOLDER)
	% Load image file and convert to greyscale if required
	% All following operations within this practical relies on using greyscale image
	originalImage = imread(imageFileName);
	if(size(originalImage, 3) > 1)
		bwImage = rgb2gray(originalImage);
	else
		bwImage = originalImage;
	end

	% Generate a random watermark, with the original filename as the seed
	% The seed value allows us to generate the same watermark for the image each time
	% Which is useful when attempting to analyse the images
	% The watermak should be of length 1000
	watermark = generateRandom1DWatermarks(1000, 1, sum(uint8(imageFileName)));
	%imwrite(watermark, [OUTPUT_FOLDER, 'watermark.bmp']);

    % Embed the watermark into the image with the specified values
    embeddedImage = embedSpreadSpectrumWatermark(bwImage, watermark, alpha, position);
    imwrite(embeddedImage, [OUTPUT_FOLDER, 'embeddedImage.bmp']);

    % Find the difference between the watermark image and the original image
    % ie It shows the watermark that was embedded
    differenceImage = abs( double(bwImage) - double(embeddedImage) );
    differenceImageTitle = sprintf('Difference Image Alpha:%.1f Position:%.0f', alpha, position);
    figure, imshow(10*uint8(differenceImage)), title(differenceImageTitle);

    % Save the greyscale and difference image
    % It wasnt requested for this experiment but its useful for the report
    imwrite(bwImage, [OUTPUT_FOLDER, 'bwImage.bmp']);
    imwrite(differenceImage, [OUTPUT_FOLDER, 'differenceImage.bmp']);
end