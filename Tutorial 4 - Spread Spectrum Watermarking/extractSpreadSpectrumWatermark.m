function [watermark] = extractSpreadSpectrumWatermark(originalImage, watermarkedImage, alpha, startPosition, watermarkSize)
	originalImage = double(originalImage);
	watermarkedImage = double(watermarkedImage);
    endPosition = startPosition+watermarkSize-1;

    % Find the DCT components of both images
	dct2DOriginalImage = dct2(originalImage);
	dct2DWatermarkedImage = dct2(watermarkedImage);

	% Turn them both into a 1D array
	dct1DOriginalImage = zigzag2dto1d(dct2DOriginalImage);
	dct1DWatermarkedImage = zigzag2dto1d(dct2DWatermarkedImage);

	% Extract the coefficents of both images at the specified position
	dctOriginalCoefficients = dct1DOriginalImage(startPosition:endPosition);
	dctWatermarkedCoefficients = dct1DWatermarkedImage(startPosition:endPosition);

	% Retrieve the 'difference' excluding the alpha value, of both images DCT coefficents
	% To retrieve the original embedded watermark. Its not gonna be exactly the same
	watermark = (dctWatermarkedCoefficients-dctOriginalCoefficients)./(alpha*dctOriginalCoefficients);

end
