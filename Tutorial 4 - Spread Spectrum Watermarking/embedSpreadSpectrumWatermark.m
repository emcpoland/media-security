function [embeddedImage] = embedSpreadSpectrumWatermark(image, watermark, alpha, startPosition)
    image = double(image);
    endPosition = startPosition+size(watermark,1)-1;

    % Retrieve the DCT components as 1D array at the reqeusted positions
    dct2DImage = dct2(image);
    dct1DImage = zigzag2dto1d(dct2DImage);
    dctCoefficients = dct1DImage(startPosition:endPosition);

    % Alter the coefficents depending on alpha value and the watermark
    embedCoefficients = dctCoefficients.*(1+alpha*watermark);
    % Embed the new coefficents at the required position,
    dct1DImage(startPosition:endPosition) = embedCoefficients;

    % Convert the adjusted DCT coefficents back into a 2D image
    embeddedDCT2DImage = dezigzag1dto2d(dct1DImage);
    embeddedImage = idct2(embeddedDCT2DImage);
    embeddedImage = uint8(embeddedImage);
end
