function [embeddedPSNR] = experimentTwo(originalImage, embeddedImage)
    embeddedPSNR = calculateFidelityMeasure(originalImage, embeddedImage);
end
