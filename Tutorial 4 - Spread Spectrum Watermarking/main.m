clear; % Clear workspace
close all; % Close previously opened figures
mkdir('output'); % Setup directory structure
%pkg load image signal nan % Import octave packages

% The script was originally written for octave,
% Some minor modifications were required to get it to work for MatLab
% Also noticed the results were ever so slightly different
% The values in the report uses the Matlab results.

% Run each experiment for multiple values of the watermark embedder
 watermarkValues = [
    {'WmIm_01_2', 0.1 2};
    {'WmIm_05_2', 0.5 2};
    {'WmIm_1_2', 1, 2};
    {'WmIm_05_10', 0.5 10};
    {'WmIm_05_100', 0.5 100};
    {'WmIm_05_1000', 0.5 1000};
    ].'; %Transpose as matlab iterates by columns not rows

for wV = watermarkValues
    name = wV{1}; alpha = wV{2}; position = wV{3};
    fprintf('Running experiments for Alpha:%.1f Position:%.0f\n\n', alpha, position)

	%Setup the output folder
	OUTPUT_FOLDER = getOutputFolder(name);

	[bwImage, watermark, embeddedImage] = experimentOne('input/baboon256.bmp', alpha, position, OUTPUT_FOLDER);

	embeddedPSNR = experimentTwo(bwImage, embeddedImage)

    experimentThree(bwImage, watermark, embeddedImage, alpha, position, OUTPUT_FOLDER);

	fprintf('\n\n\n')
end
