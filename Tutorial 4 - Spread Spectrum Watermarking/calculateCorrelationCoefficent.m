function [correlationCoefficent] = calculateCorrelationCoefficent(originalSubject, testSubject)
	originalSize = size(originalSubject, 1);

	% Generate 1000 unrelated random sample watermarks
	generatedSamples = generateRandom1DWatermarks(originalSize, 1000, rand('seed'));

    % Insert reference watermark at position 250
    % Expect a single peek at position 250, if originalSubject is the same as the testSubject
	generatedSamples(:, 250) = originalSubject;

	% Match formula on Tutorial 1, Slide 12
	normalisedSubject = sqrt(dot(testSubject, testSubject)); % norm(testSubject)

	% Iterate over each generated sample
	for i = 1:1000
		% Gonna compare the test subject to this randomly generated sample
		% It should have a high correlation at 250, as we inserted our original sample in at that point
		compareSubject = generatedSamples(:,i);

		% Match formula on Tutorial 1, Slide 12.
		% There was a typo at this point in the practical handout. I believe it was missing ' when it was essentially calculating the dot product
		% It generates the same results as corrcoef, however its bounded to <1, whereas our solution can go >1, not sure of the difference
		correlationCoefficent(i) = dot(testSubject, compareSubject)/normalisedSubject;
	end

end
