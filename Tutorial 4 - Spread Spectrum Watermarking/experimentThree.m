function experimentThree(originalImage, watermark, embeddedImage, alpha, position, OUTPUT_FOLDER)
	% Extract the watermark from the image
	watermarkSize = numel(watermark);
	extractedWatermark = extractSpreadSpectrumWatermark(originalImage, embeddedImage, alpha, position, watermarkSize);

	% Calculate the CorrelationCoefficient of the original watermark to the extracted watermark
	extractedWatermarkCorrelationCoefficient = calculateCorrelationCoefficent(watermark, extractedWatermark);

	% Display the Original <-> Extracted Watermark Correlation Coefficents
    extractedPlotTitle = sprintf('Original<->Extracted Watermark Correlation Coefficents Alpha:%.1f Position:%.0f', alpha, position);
	figure, plot(extractedWatermarkCorrelationCoefficient), title(extractedPlotTitle), print([OUTPUT_FOLDER, 'extractedWatermarkCorrelationCoefficient.eps']);

	% Calculate an unreleated random watermark, dont bother seeding this one
	unrelatedRandomWatermark = generateRandom1DWatermarks(1000, 1, rand('seed'));
	unrelatedWatermarkCorrelationCoefficient = calculateCorrelationCoefficent(unrelatedRandomWatermark, extractedWatermark);

	% Display the Unrelated <-> Extracted Watermark Correlation Coefficents
    unrelatedPlotTitle = sprintf('Unrelated<->Extracted Watermark Correlation Coefficents Alpha:%.1f Position:%.0f', alpha, position);
	figure, plot(unrelatedWatermarkCorrelationCoefficient), title(unrelatedPlotTitle), print([OUTPUT_FOLDER, 'unrelatedWatermarkCorrelationCoefficient.eps']);

end
