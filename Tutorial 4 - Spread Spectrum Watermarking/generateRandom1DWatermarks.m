function [watermark] = generateRandom1DWatermarks(mSize, numberOfSamples, seed)
    % Save current seed value so we can reset it later
    % Essentially scope the seed value
    % Sets the new seed value
    % Which allows us to consistently generate the same 'random' watermark
    % Should make this an optional input argument
    original_seed = rand('seed');
    rand('seed', seed);

    % Generate random 1D array with a value between 0<->1 for each requested watermark
    % Its faster to generate all the watermarks now, than to iteratively call this functions for multiple watermarks
    watermark = rand(mSize, numberOfSamples);

    % We need each watermark to conform to a specific property, hence loop over each watermark
    for i=1:numberOfSamples
        % The pracitcal never suggested doing this, however it achieves much better results
        % and is required as per "Secure Spread Spectrum Watermarking for Multimedia, pg 1678, IV Structure Of The Watermark"
        % We have several options, we could create a watermark
        % where x:{0,1} etc
        % or x:R where the watermark has 0 mean and unit variance N(0,1)

        % x:{0,1}
        %watermark(find(watermark <= 0.5)) = 0;
        %watermark(find(watermark > 0.5)) = 1;

        % Instead use N(0,1) as x:{0,1} is vulnerable to superimposing other watermarks
        watermark(:, i) = watermark(:, i)-mean2(watermark(:, i));
        watermark(:, i) = watermark(:, i)/std2(watermark(:, i));
    end

    % Reset the seed value
    rand('seed', original_seed);
end
