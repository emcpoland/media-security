\documentclass{article}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{lscape}
\usepackage{hhline}
\usepackage{float}

\graphicspath{ {fig/} }

\setlength{\parindent}{0pt}
\setlength{\parskip}{1em}

\begin{document}

\lstset{
	language=Matlab,
	caption=\lstname,
	basicstyle=\small,
	breaklines=true,
    morekeywords={matlab2tikz}
}

\title{Practical Class 4: Spread Spectrum Watermarking}
\author{Media Security CSC4066\\Dr Fatih Kurugollu\\\\Emmet McPoland - 40060420}
\date{1st November 2015\\}

\maketitle
\renewcommand{\abstractname}{}
\begin{abstract}
The objective of the following document is to investigate the impact on the fidelity of an embedded image, and its relationship with the strength and position parameters of Coxs Spread Spectrum watermarking method. Along with the effects of the strength and position parameters on the detection range during watermark correlation.
Included in the report are the results of the embedding process using various input parameters on a greyscale image. The source code used to carry out the process has also been included and may be found within Appendix \ref{app:scripts}.
\end{abstract}

\pagebreak
\section*{Effects of Alpha and Position on PSNR}
\begin{center}
    \def\arraystretch{2}
    \begin{table}[h]
        \centering
        \caption{Effects of the Alpha parameter on the PSNR}
        \begin{tabular}{| c | c | c |} \hline
            Alpha & Position & Embedded PSNR \\ \hhline{|=|=|=|}
            0.1 & 2 & 38.134 \\ \hline
            0.5 & 2 & 24.190 \\ \hline
            1.0 & 2 & 18.181 \\ \hline
        \end{tabular}
        \label{tbl:psnrAlpha}
    \end{table}
\end{center}
%
As shown in Table \ref{tbl:psnrAlpha}, as the Alpha value increases, the PSNR decreases. Which signifies that a larger Alpha causes a greater degradation in the quality of the image. This degradation can be seen in  Figure \ref{fig:embedAlpha}.
%
\begin{center}
    \def\arraystretch{2}
    \begin{table}[h]
        \centering
        \caption{Effects of the Insert Position parameter on the PSNR}
        \begin{tabular}{| c | c | c |} \hline
            Alpha & Position & Embedded PSNR \\ \hhline{|=|=|=|}
            0.5 & 10 & 24.387 \\ \hline
            0.5 & 100 & 30.866 \\ \hline
            0.5 & 1000 & 35.776 \\ \hline
        \end{tabular}
        \label{tbl:psnrPosition}
    \end{table}
\end{center}
%
As shown in Table \ref{tbl:psnrPosition}, as the Position value increases, the PSNR also increases. Which signifies that a lower Position value causes a greater degradation in the quality of the image. This degradation can be seen in Figure \ref{fig:embedPosition}.

\newpage
\section*{Effects of Alpha and Position on detection range}

The detection range is equal to the difference between the detection threshold and the detected spike of the reference watermark. The detection threshold is set based on an estimated value, of which any value greater than the threshold is said to signify the existence of the reference watermark. Through observing Figures \ref{fig:extractAlpha} and \ref{fig:extractPosition} this can loosely be set to the value in which the non-peak values peak.

Figures \ref{fig:extractAlpha} and \ref{fig:extractPosition}, show the plots of the correlation coefficients of the reference watermark and the extracted watermark with the various embedding parameters. The watermark used during the embedding process was placed at the 250th position. Figures \ref{fig:unrelatedAlpha} and \ref{fig:unrelatedPosition} demonstrates the case when the reference watermark does not exist within the image. The remaining figures demonstrate the effects of the alpha and position parameters on the detection range. Enlarged versions of the figures are available in Appendix \ref{app:correlationCoefficientsBig}.

\begin{figure}[h]
    \centering
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_01_2/unrelatedWatermarkCorrelationCoefficient.eps}
        \caption[]{{\small Unrelated}}
        \label{fig:unrelatedAlpha}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_01_2/extractedWatermarkCorrelationCoefficient.eps}
        \caption[]{{\small Alpha 0.1 Position 2}}
        \label{fig:WmIm_01_2_Extracted}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_05_2/extractedWatermarkCorrelationCoefficient.eps}
        \caption[]{{\small Alpha 0.5 Position 2}}
        \label{fig:WmIm_05_2_Extracted}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_1_2/extractedWatermarkCorrelationCoefficient.eps}
        \caption[]{{\small Alpha 1.0 Position 2}}
        \label{fig:WmIm_1_2_Extracted}
    \end{subfigure}
    \caption[] {\small Effects of the Alpha parameter on the detection range}
    \label{fig:extractAlpha}
\end{figure}

The spikes in Figures \ref{fig:WmIm_01_2_Extracted}, \ref{fig:WmIm_05_2_Extracted}, \ref{fig:WmIm_1_2_Extracted}, \ref{fig:WmIm_05_10_Extracted}, \ref{fig:WmIm_05_100_Extracted}, \ref{fig:WmIm_05_1000_Extracted} signify that the extracted watermark has high correlation with the embedded watermark which was placed at the 250th position. They also show that medium values for the Alpha and Position parameters achieve the greatest detection range and are therefore more desirable. Furthermore it can be said that the lower Position value of 10 achieves a better range than that of the higher value of 1000. While the higher alpha value of 1 achieves a better range than the lower value of 0.1. This is of significant importance as while a larger Alpha value caused a greater degradation in the quality of the image it did not directly transfer into an increase in robustness of the watermark, therefore the value for Alpha must be intelligently chosen. It could also be stated that the selection of DCT coefficients could be greatly improved, by either selecting which coefficients to modify through the magnitude of the coefficient or by taking into account the properties
of the Human Visual System, as opposed to arbitrary selecting coefficients which are adjacent to each other. As there is no direct link between the position of the coefficient and the robustness of the watermarking.

\begin{figure}[h]
    \centering
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_05_10/unrelatedWatermarkCorrelationCoefficient.eps}
        \caption[]{{\small Unrelated}}
        \label{fig:unrelatedPosition}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_05_10/extractedWatermarkCorrelationCoefficient.eps}
        \caption[]{{\small Alpha 0.5 Position 10}}
        \label{fig:WmIm_05_10_Extracted}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_05_100/extractedWatermarkCorrelationCoefficient.eps}
        \caption[]{{\small Alpha 0.5 Position 100}}
        \label{fig:WmIm_05_100_Extracted}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_05_1000/extractedWatermarkCorrelationCoefficient.eps}
        \caption[]{{\small Alpha 0.5 Position 1000}}
        \label{fig:WmIm_05_1000_Extracted}
    \end{subfigure}
    \caption[] {\small Effects of the Insert Position parameter on the detection range}
    \label{fig:extractPosition}
\end{figure}

As a final note the same program was executed on both Matlab and Octave, producing two different results. On Octave the above result was produced, in which Figure \ref{fig:WmIm_05_1000_Extracted} showed less correlation than Figure \ref{fig:WmIm_05_100_Extracted}. Where as on Matlab Figure \ref{fig:WmIm_05_1000_Extracted} showed a higher correlation than Figure \ref{fig:WmIm_05_100_Extracted}, as shown in Appendix \ref{app:matlabDiscrepancy}. Despite this, in general the results were similar although Octave did produce higher magnitudes of correlation compared to Matlab across the board. The observed discrepancies require further investigation.

\section*{Conclusion}
In conclusion, the value of Alpha and the choosing of which coefficients to modify, should be based on the properties of an individual image. A higher Alpha value does not always yield better correlation with the original watermark, but does result in a greater degradation of quality. The decision to select DCT coefficients adjacent to each other is a naive approach, which could be improved by selecting the largest coefficients instead. In the case documented, as the position value decreases the embedded image degrades in quality. Ultimately the fidelity of the watermarked image does not necessarily have a bearing on the robustness of the watermark. Issues regarding compatibility between Octave and Matlab have also been highlighted which require further investigation.

\appendix
\pagebreak
\section{Results}

\subsection{Images}

\subsubsection {Varying the alpha value}
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_01_2/bwImage.png}
        \caption[]{{\small Original}}
        \label{fig:originalAlphaEmbedded}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_01_2/embeddedImage.png}
        \caption[]{{\small Alpha 0.1 Position 2}}
        \label{fig:WmIm_01_2_Embedded}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_05_2/embeddedImage.png}
        \caption[]{{\small Alpha 0.5 Position 2}}
        \label{fig:WmIm_05_2_Embedded}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_1_2/embeddedImage.png}
        \caption[]{{\small Alpha 1.0 Position 2}}
        \label{fig:WmIm_1_2_Embedded}
    \end{subfigure}
    \caption[] {\small The embedded image as the alpha value increases}
    \label{fig:embedAlpha}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_01_2/bwImage.png}
        \caption[]{{\small Original}}
        \label{fig:originalAlphaDifference}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_01_2/differenceImage.png}
        \caption[]{{\small Alpha 0.1 Position 2}}
        \label{fig:WmIm_01_2_Difference}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_05_2/differenceImage.png}
        \caption[]{{\small Alpha 0.5 Position 2}}
        \label{fig:WmIm_05_2_Difference}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_1_2/differenceImage.png}
        \caption[]{{\small Alpha 1.0 Position 2}}
        \label{fig:WmIm_1_2_Difference}
    \end{subfigure}
    \caption[] {\small The difference image as the alpha value increases}
    \label{fig:differenceAlpha}
\end{figure}

\subsubsection {Varying the position value}
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_05_10/bwImage.png}
        \caption[]{{\small Original}}
        \label{fig:originalAlpha}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_05_10/embeddedImage.png}
        \caption[]{{\small Alpha 0.5 Position 10}}
        \label{fig:WmIm_05_10_Embedded}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_05_100/embeddedImage.png}
        \caption[]{{\small Alpha 0.5 Position 100}}
        \label{fig:WmIm_05_100_Embedded}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_05_1000/embeddedImage.png}
        \caption[]{{\small Alpha 0.5 Position 1000}}
        \label{fig:WmIm_05_1000_Embedded}
    \end{subfigure}
    \caption[] {\small The embedded image as the position value increases}
    \label{fig:embedPosition}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_05_10/bwImage.png}
        \caption[]{{\small Original}}
        \label{fig:originalPositionDifference}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_05_10/differenceImage.png}
        \caption[]{{\small Alpha 0.5 Position 10}}
        \label{fig:WmIm_05_10_Difference}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_05_100/differenceImage.png}
        \caption[]{{\small Alpha 0.5 Position 100}}
        \label{fig:WmIm_05_100_Difference}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{./output/WmIm_05_1000/differenceImage.png}
        \caption[]{{\small Alpha 0.5 Position 1000}}
        \label{fig:WmIm_05_1000_Difference}
    \end{subfigure}
    \caption[] {\small The difference image as the position value increases}
    \label{fig:differencePosition}
\end{figure}

\subsubsection {Larger Figures of Correlation Coefficients plot}
\label{app:correlationCoefficientsBig}

\begin{figure}[H]
    \centering
    \includegraphics[angle=90]{./output/WmIm_01_2/unrelatedWatermarkCorrelationCoefficient.eps}
    \caption[]{{\small Unrelated}}
    \label{fig:unrelatedAlphaBig}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[angle=90]{./output/WmIm_01_2/extractedWatermarkCorrelationCoefficient.eps}
    \caption[]{{\small Alpha 0.1 Position 2}}
    \label{fig:WmIm_01_2_ExtractedBig}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[angle=90]{./output/WmIm_05_2/extractedWatermarkCorrelationCoefficient.eps}
    \caption[]{{\small Alpha 0.5 Position 2}}
    \label{fig:WmIm_05_2_ExtractedBig}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[angle=90]{./output/WmIm_1_2/extractedWatermarkCorrelationCoefficient.eps}
    \caption[]{{\small Alpha 1.0 Position 2}}
    \label{fig:WmIm_1_2_ExtractedBig}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[angle=90]{./output/WmIm_05_10/unrelatedWatermarkCorrelationCoefficient.eps}
    \caption[]{{\small Unrelated}}
    \label{fig:unrelatedPositionBig}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[angle=90]{./output/WmIm_05_10/extractedWatermarkCorrelationCoefficient.eps}
    \caption[]{{\small Alpha 0.5 Position 10}}
    \label{fig:WmIm_05_10_ExtractedBig}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[angle=90]{./output/WmIm_05_100/extractedWatermarkCorrelationCoefficient.eps}
    \caption[]{{\small Alpha 0.5 Position 100}}
    \label{fig:WmIm_05_100_ExtractedBig}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[angle=90]{./output/WmIm_05_1000/extractedWatermarkCorrelationCoefficient.eps}
    \caption[]{{\small Alpha 0.5 Position 1000}}
    \label{fig:WmIm_05_1000_ExtractedBig}
\end{figure}

\subsubsection {Matlab Discrepancy}
\label{app:matlabDiscrepancy}
\begin{figure}[H]
    \centering
    \includegraphics[angle=90, trim=0cm 0cm 0cm 8cm]{./output/Matlab/WmIm_05_10/extractedWatermarkCorrelationCoefficient.eps}
    \caption[]{{\small Alpha 0.5 Position 10}}
    \label{fig:WmIm_05_10_ExtractedBig}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[angle=90, trim=0cm 0cm 0cm 8cm]{./output/Matlab/WmIm_05_100/extractedWatermarkCorrelationCoefficient.eps}
    \caption[]{{\small Alpha 0.5 Position 100}}
    \label{fig:WmIm_05_100_ExtractedBig}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[angle=90, trim=0cm 0cm 0cm 8cm]{./output/Matlab/WmIm_05_1000/extractedWatermarkCorrelationCoefficient.eps}
    \caption[]{{\small Alpha 0.5 Position 1000}}
    \label{fig:WmIm_05_1000_ExtractedBig}
\end{figure}

\pagebreak
\subsection{Console}
\lstinputlisting{./output/console.txt}

\pagebreak
\section{Source Files}
\label{app:scripts}
\begin{landscape}

\lstinputlisting{../main.m}

\lstinputlisting{../getOutputFolder.m}

\pagebreak
\lstinputlisting{../experimentOne.m}

\pagebreak
\lstinputlisting{../experimentTwo.m}

\pagebreak
\lstinputlisting{../experimentThree.m}

\pagebreak
\lstinputlisting{../generateRandom1DWatermarks.m}

\pagebreak
\lstinputlisting{../embedSpreadSpectrumWatermark.m}

\pagebreak
\lstinputlisting{../extractSpreadSpectrumWatermark.m}

\pagebreak
\lstinputlisting{../calculateFidelityMeasure.m}

\pagebreak
\lstinputlisting{../calculateCorrelationCoefficent.m}

\pagebreak
\lstinputlisting{../zigzag2dto1d.m}
\lstinputlisting{../dezigzag1dto2d.m}
\lstinputlisting{../zigzag4.m}
\end{landscape}

\end{document}
