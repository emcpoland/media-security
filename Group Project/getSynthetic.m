function [s] = getSynthetic(pw, ph, N, px, qx, py, qy)
	% Add border elements onto probabilty map size
    % They will be removed at the end again
	w = pw+2*N;
	h = ph+2*N;

    % Infer py,qy if not given
	if ~exist('py', 'var') py = px; end
	if ~exist('qy', 'var') qy = qx; end

	M = [qx/px 0; 0 qy/py];
	s = zeros(w, h);

    % Calculate the distance of each point after its been transformed
    % To the nearest integer posiiton in the lattice
	for x=1:w
		for y=1:h
			transformed_coords = M*[x;y];
			nearest_integer_coords = floor(transformed_coords+0.5);
			distance_off = norm(transformed_coords-nearest_integer_coords);
			s(x, y) = distance_off;
		end
	end

	s = s(N+1:end-N, N+1:end-N); % Remove border elements
end
