function [y] =  linearlyResampleImageLooping(x, px,qx, py,qy)
	if ~exist('py', 'var') py = px; end
	if ~exist('qy', 'var') qy = qx; end

	[height, width]= size(x);
	for r=1:height
		resampledWidth(r,:) = linear_resample_signal_loop(x(r,:), px,qx);
	end
	x = resampledWidth;

	[height, width]= size(x);
	for c=1:width
		resampledHeight(:,c) = linear_resample_signal_loop(x(:,c)', py,qy)';
	end
	x = resampledHeight;

	y = uint8(x);
end
