function [P] =  EM_1D(y)
	%%%%%%%%% Binomial low pass filter with Nh set to 3, Not required
	%h = [3, 3]; %% [Nh, Nh]
	%binomialCoeff = conv(h,h);
	%for n = 1:4 % Several iterations
	%    binomialCoeff = conv(binomialCoeff,h);
	%end

	y = y/max(y);

	%%%%%%% Build up list of neighbours for each non-boundary element
	N = 2; % %%%%%% Neighborhood Size = 5, 2 Neighbors on either side

	%%%%%%% Build list of each neighbourhood including the i element
	Yn = [];
	for i=N+1:length(y)-N
		Yn = [Yn; y(i-N:i+N)];
	end

	%%%%%%% Remove the i element to get Y
	Y = Yn;
	Y(:,N+1) = [];

	y = y(N+1:end-N); %%%%% Remove boundary elements, no longer needed
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	alpha(1, 1:size(Y,2)) = rand(1,1);
	v = [0.75];
	p0 = 1/length(y);
	epsilonThreshold = 0.00001;

	n = 1;
	while true

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Expectation Step
		for i=1:length(y)
			R(i) = abs(y(i)-sum(alpha(n,:).*Y(i,:)));
		end

		%%%% Additional filter to help tend towards closing condition
		%R = filter([3, 3], 1, R); % Filter had minimal effect in most cases during experimentation

		for i=1:length(y)
			P(i) = (1/(v(n)*sqrt(2*pi)))*exp((-R(i)^2)/(2*v(n)^2));
			w(i) = P(i)/(P(i)+p0);
		end

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Maximisation Step
		W = diag(w);

		v(n+1) = (sum(w.*(R.^2))/sum(w))^(1/2);
		nxtAlpha = inv(Y'*W*Y)*Y'*W*y';
		nxtAlpha = nxtAlpha'; %Retranpose again
        
		alpha = [alpha; nxtAlpha];

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Loop & Convergence Check
	 	n=n+1;
	 	epsilon = norm(alpha(n,:)-alpha(n-1,:));
	 	epsilonThreshold;
		if epsilon < epsilonThreshold
            break
        end
    end
    P = P/max(P); % Limit 0<=P<=1
end
