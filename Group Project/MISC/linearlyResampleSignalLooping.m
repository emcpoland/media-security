function [y] =  linearlyResampleSignalLooping(x, p, q)
	m = length(x);
	n = m*p/q;

	for t=1:m
		xu(p*t-1) = x(t);
	end

	A = [0.5 1 0.5];
	A = [1/p:1/p:1, 1-1/p:-1/p:1/p];
	xi = conv(xu, A);

	for t=1:n
		xd(t) = xi(q*t);
	end

	y = xd;
	y = y(2:end);
end
