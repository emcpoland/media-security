clear; % Clear workspace
close all; % Close previously opened figures
mkdir('output'); % Setup directory structure
%pkg load signal image;
%pkg load statistics;

signal = randperm(500,10);
resample_signal = resample(signal, 2,1);% 'linear'
my_resample_signal = linearlyResampleSignal(signal, 2,1);

% Plot the 2 vectors above each other, using the largest x scale
figure;
ax1 = subplot(3,2,1); % top subplot
ax2 = subplot(3,2,3); % middle subplot
ax3 = subplot(3,2,5); % bottom subplot

plot(ax1, signal);
plot(ax2, resample_signal);
plot(ax3, my_resample_signal);
%linkaxes([ax3,ax2],'xy');

bx1 = subplot(3,2,2); % top subplot
bx2 = subplot(3,2,4); % middle subplot
bx3 = subplot(3,2,6); % bottom subplot

stem(bx1, EM_1D(signal));
stem(bx2, EM_1D(resample_signal));
stem(bx3, EM_1D(my_resample_signal));
%linkaxes([bx3,bx2],'xy');
