function [synMaps] = getSyntheticMaps(outputDir, psize, pq, N)
    synMaps = [];
    for i=1:length(pq)
        p = pq(i,1); q = pq(i,2);

        % Generate the synthetic map for a given p q
        syn = getSynthetic(psize(1), psize(2), N, p, q);

        % Save Map and FFT
        %syn = uint8(s/max(max(s))*255);
        imwrite(syn, sprintf('%s/%i_%i.tiff', outputDir, p,q));
        imwrite(getFFT(syn), sprintf('%s/fft_%i_%i.tiff', outputDir, p,q));

        % Hand back all maps
        synMaps = cat(3, synMaps, syn);
    end
end

