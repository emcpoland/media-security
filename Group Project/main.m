clear; close all;
%pkg load image;
mkdir('output/synthetic');
mkdir('output/probability');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%pq = [2 1; 4 3];
%pq = [2 1; 1 2; 2 2; 3 2; 1 3; 2 3; 4 3; 5 3; 1 4; 3 4; 5 4; 7 4; 1 5; 2 5; 3 5; 4 5; 6 5; 7 5; 8 5; 9 5]
pq = [6 5; 5 4; 4 3; 7 5; 3 2; 8 5; 5 3; 7 4; 9 5; 2 1;] %1 5; 1 4; 1 3; 2 5; 1 2; 3 5; 2 3; 3 4; 4 5; 1 1;

s = 1, e = 2; %Start and end position for image files

numberOfImages = e-s+1
numberOfChecks = length(pq);

N = 2 %Number of neighbours to either side

% Generate the similarity values for unmodified images, inorder to set the threshold
origProbMaps = getProbabilityMaps('./output/original', './output/probability', s, e, N);
origSynMaps = getSyntheticMaps('./output/synthetic', size(origProbMaps(:,:,1)), pq, N);
origSimValues = calculateSimilarityMeasures(origProbMaps, origSynMaps);

threshold = max(max(origSimValues));

% Initiliase ararys inorder to hold the results
successRates = zeros(1,numberOfChecks);
actual = zeros(numberOfImages, numberOfChecks);

% Loop over each resampling factor
for i=1:numberOfChecks
    p = pq(i,1); q = pq(i,2);
    
    % Setup directory structure
    resampledDir = sprintf('./output/%d_%d/resampled', p, q);
    synDir = sprintf('./output/%d_%d/synthetic/', p, q);
    probDir = sprintf('./output/%d_%d/probability/', p, q);
    mkdir(synDir);
    mkdir(probDir);

    % Find the simlarity values of the image set for each resampling factor
    resampledProbMaps = getProbabilityMaps(resampledDir, probDir, s, e, N);
    resampledSynMaps = getSyntheticMaps(synDir, size(resampledProbMaps(:,:,1)), pq, N);
    resampledSimValues = calculateSimilarityMeasures(resampledProbMaps, resampledSynMaps);

    % Loop over similarity values determining if they are above the threshold
    % Calculate the failure rate, which is proportional to the number of
    % Similarity values which fall beneath the threshold
    failures = 0;
    for n=1:numberOfImages
        actual(n,i) = max(resampledSimValues(n,:));
        if(actual(n,i) > threshold)
            fprintf('Image %d (%d/%d) has been successfully detected as tampered as %f > %f\n', n, p, q, actual(n,i), threshold);
        else
            fprintf('Image %d (%d/%d) failed to be detected as tampered as %f <= %f\n', n, p, q, actual(n,i), threshold);
            failures = failures+1;
        end
    end
    successRates(i) = ((numberOfImages-failures)/numberOfImages)*100;
    fprintf(sprintf('%f success rate for (%d/%d)\n', successRates(i), p, q));
end

% Plot a graph of the resampling rate against the success rate
pqLabel = {'6/5', '5/4', '4/3', '7/5', '3/2', '8/5', '5/3', '7/4', '9/5', '2/1'} %'1/5', '1/4', '1/3', '2/5', '1/2', '3/5', '2/3', '3/4', '4/5', '1', 
xTick = 1:length(successRates);

figure, plot(xTick, successRates);
set(gca, 'Xtick', xTick, 'XtickLabel', pqLabel);
ylim([0,100])

% Plot the graph showing the destribution of similarity values for each resampling rate
% And display the threshold it must meet inorder to qualify as a success
figure, hold on;
boxplot(actual);
set(gca, 'Xtick', xTick, 'XtickLabel', pqLabel);
refline([0, threshold]);
hold off;