function [P, alpha] =  EM(y, N, v, epsilonThreshold)
	% Binomial low pass filter with Nh set to 3, Not required
	%h = [3, 3]; %% [Nh, Nh]
	%binomialCoeff = conv(h,h);
	%for n = 1:4 % Several iterations
	%    binomialCoeff = conv(binomialCoeff,h);
	%end
	inputSize = size(y);

	% Build list of each neighbourhood, including the i element
	Yn = [];
	for j=N+1:size(y,2)-N
		for i=N+1:size(y,1)-N
			submatrix = y(j-N:j+N, i-N:i+N);
			submatrix = reshape(submatrix',1, numel(submatrix));
			Yn = [Yn; submatrix];
		end
	end

	% Remove the i element to get Y
	Y = Yn;
	Y(:,ceil(end/2)) = [];

	y = y(N+1:end-N, N+1:end-N); % Remove boundary elements, as they are no longer needed
	y = reshape(y', numel(y), 1); % Reshape y into vector for multiplication later on
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	alpha(1:size(Y,2)) = rand(); % Initial random alpha value for each neighbour of i
	p0 = 1/(max(y)-min(y)); %1/numel(y); %1/255 %1 % Unsure as to what defines 'range'

	while true

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Expectation Step
		for i=1:size(Yn,1)
            % It would seem appropiate to divide the sum, inorder to obtain
            % An average, but apparently not /numel(Y(i,:))
			R(i) = abs(Yn(i,ceil(end/2))-sum(alpha.*Y(i,:)));
		end

		%%%% Additional filter to help tend towards closing condition
		%R = filter([3, 3], 1, R); % Filter had minimal effect in most cases during experimentation

		for i=1:length(R)
			P(i) = (1/(v*sqrt(2*pi)))*exp((-R(i)^2)/(2*v^2));
			w(i) = P(i)/(P(i)+p0);
			% If the data isnt related then w(i) = 0 which causes all sorts of issues
			% The issue is exp(R^2) returns 0 which makes P =0, which is fine,.
            % However if P = 0 then w = 0, and it wont be possible to find the inverse
            % Hence it may be reasonable to reset w to a known value if it occurs
			% if w(i) == 0 w(i) = 1%eps; end
		end

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Maximisation Step
		W = diag(w);
		nxt_alpha = (inv(Y'*W*Y)*Y'*W*y)';

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Loop & Convergence Check
	 	epsilon = norm(nxt_alpha-alpha);
		if epsilon > epsilonThreshold
			v = (sum(w.*(R.^2))/sum(w))^(1/2);
			alpha = nxt_alpha;
		else
			break
		end
    end
    
	P = P./max(P); % Limit 0<=P<=1
	P = reshape(P, inputSize-2*N)'; % Reshape into a 2d shape

    % Reshape alpha adding missing 0 element back for display purposes
	alpha =  [alpha(1:end/2), 0, alpha(end/2+1:end)];
	alpha = reshape(alpha, 2*N+1, 2*N+1);

end
