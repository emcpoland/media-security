function [probMaps] = getProbabilityMaps(inputDir, outputDir, s, e, N)
    probMaps = [];
    
    % Loop over each image obtaining the probability map through the EM algo
    % And hand back all the probability images that were generated
    for imageNumber = s:e
        img = imread(sprintf('%s/%d.tiff', inputDir, imageNumber));

        [prob, alpha] = EM(im2double(img), N, 0.75, 0.00001);
        imwrite(prob, sprintf('%s/%d.tiff', outputDir, imageNumber));
        imwrite(getFFT(prob), sprintf('%s/fft_%d.tiff', outputDir, imageNumber));

        probMaps = cat(3, probMaps, prob);
    end
end
