function [y] =  linearResampleSignal(x, p, q)
	x = x'; % Turn signal into vector
	[mX, nX] = size(x);

	% Generate the A matrix
    A = [];
	for i = 1:p-1
		A(i,i:i+1) = [i*(1/p), 1-i*(1/p)];
	end
	A = [zeros(1,p);A];
	A(1:1) = 1;

    % Initialise matrices for improved speed
	[mA, nA] = size(A);
	B = zeros(floor(mX*(p/q)), mX);

    % Replicate A into B for the size of the signal
	j = 1;
	for i= 1:nA-1:mX
		B(j:j+mA-1, i:i+nA-1) = A;
		j=j+mA;
    end

    % Remove excess elements which cause black borders
	B = B(1:floor(mX*(p/q)), 1:mX); 

    % Apply the transformation
	y = B*x;

	y = y'; % Turn vector back into array
end
