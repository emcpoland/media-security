function [f] = getFFT(image)
    % Several functions to improve the perceptibility of the FFT image
	f = im2double(image);
	f = fft2(f);
	f = fftshift(f);
	f = abs(f);
	f = log(f+1);

	f = f./max(max(f));
	f = f.*(f < 0.75 == 0);
end
