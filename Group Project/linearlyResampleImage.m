function [y] =  linearlyResampleImage(x, px,qx, py,qy)
    % Infer py,qy if not given
	if ~exist('py', 'var') py = px; end
	if ~exist('qy', 'var') qy = qx; end

	x = im2double(x);

    % Pass the 2d lattice in twice to resample both directions
	y = linearlyResampleSignal(x, px,qx);
	y = linearlyResampleSignal(y', py,qy)';

	y = uint8(y*255);
end
