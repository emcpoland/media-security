function [simValues] = calculateSimilarityMeasures(probMaps, synMaps)
    numberOfProbs = size(probMaps, 3);
    numberOfSyns = size(synMaps, 3);

    simValues = zeros(numberOfProbs, numberOfSyns);

    % Compare each probability maps with every synthetic map
    for p=1:size(probMaps, 3)
        for s=1:size(synMaps, 3)
            simValues(p,s) = getSimilarity(probMaps(:,:,p), synMaps(:,:,s));
        end
    end
end
