\documentclass{article}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{lscape}
\usepackage{mathtools}
\usepackage{commath}

\graphicspath{ {fig/} }

\setlength{\parindent}{0pt}
\setlength{\parskip}{1em}

\begin{document}

\lstset{
	language=Matlab,
	caption=\lstname,
	basicstyle=\small,
	breaklines=true,
    morekeywords={matlab2tikz}
}

\title{Exposing Digital Forgeries by Detecting Traces of Re-sampling}
\author{Media Security CSC4066\\Dr Fatih Kurugollu\\\\Emmet McPoland - 40060420}
\date{17th October 2015\\}

\maketitle

During the process of forging an image it is often necessary to manipulate the perspective of an image by scaling or rotating specific areas of it. When scaling and rotating, the number and positioning of the pixels in an image are modified, this is called resampling. Interpolation is then used to give each pixel its new value. For example, if an image is 10x10px in size and is scaled to 20x20px, there is an increase in 10 pixels both horizontally and vertically. A colour must be assigned to each of these new pixels. The value assigned depends on the interpolation method used, although all of them use the knowledge of the surrounding pixels to estimate the new pixel value in one way or another. It is this property of correlation of the new pixel value to its surroundings that makes it possible to analyse if an image has been resampled. A photograph taken directly from a camera without any modifications made to it should not inhibit this property. Therefore any traces of resampling is said to signify a forged image.

The following section details the implementation of the method proposed by Alin C. Popescu and Hany Farid within their paper “Exposing Digital Forgeries by Detecting Traces of Re-sampling”. While the following technique holds true for all forms of interpolation, only linear interpolation will be looked at within this report. Details regarding the specific steps mentioned in Popescu and Hanys paper are also elaborated on to provide a more thorough picture of its implementation.

\section{Resampling}

Popescu and Hany opens the paper up by discussing two different procedures which can be used to resample a one dimensional signal. The first method provides a first principle look at resampling and involves increasing the distance of each current sample by p. The signal is then convolved in order to provide interpolated values for new samples. Finally only every q element from the convolved signal is chosen to represent the final resampled signal. Through this method it is possible to upsample (increase the number of samples) or downsample (decrease the number of samples).

The other method described within the paper is similar in nature, however the spaces and convolution is represented by a single transformation matrix A. The matrix is then multiplied against the signal vector to produce the resampled signal. The ability to represent the entire resampling process into a single matrix, along with the increase performance provided by matrix arithmetic in modern day computers makes this the preferred option of the two discussed.

Equation \ref{eq:A} shows how the matrix $A$ is formed providing you have the factors p,q and wish to scale the signal by $p \over q$.
\begin{equation}
	A =
	\begin{pmatrix}
	1 & 0 & 0 & 0\dots \\
	1/p & 1-(1/p) & 0 & 0\dots \\
	0 & 2/p & 1-(2/p) & 0\dots\\
	\ddots & \ddots & \ddots & \ddots \\
	\dots & 0 & (p-1)/2 & 1-((p-1)/p) \\
	\dots & 0 & 0 & 1 \\
	\end{pmatrix}
	\Longrightarrow
	\begin{pmatrix}
	1 & 0 & 0 & 0 \\
	0.25 & 0.75 & 0 & 0 \\
	0 & 0.5 & 0.5 & 0\\
	0 & 0 & 0.75 & 0.25 \\
	0 & 0 & 0 & 1 \\
	\end{pmatrix}
	\label{eq:A}
\end{equation}

However $A$ must have a width equal to the number of samples within the original signal $\vec{x}$ in order to be able to multiply the matrix with the vector. To resolve this the pattern shown within Equation \ref{eq:A}, is repeated as shown in Equation \ref{eq:Am}.

\begin{equation}
	A =
	\begin{pmatrix}
	1 & 0 & 0 & 0 \\
	0.25 & 0.75 & 0 & 0 \\
	0 & 0.5 & 0.5 & 0 \\
	0 & 0 & 0.75 & 0.25 \\
	\end{pmatrix}
	\Longrightarrow
	\underbrace{
		\begin{pmatrix}
		1 & 0 & 0 & 0 & 0 & 0 & 0 \\
		0.25 & 0.75 & 0 & 0 & 0 & 0 & 0 \\
		0 & 0.5 & 0.5 & 0 & 0 & 0 & 0 \\
		0 & 0 & 0.75 & 0.25 & 0 & 0 & 0 \\
		0 & 0 & 0 & 1 & 0 & 0 & 0 \\
		0 & 0 & 0 & 0.25 & 0.75 & 0 & 0 \\
		0 & 0 & 0 & 0 & 0.5 & 0.5 & 0 \\
		0 & 0 & 0 & 0 & 0 & 0.75 & 0.25 \\
		\end{pmatrix}
		}_\text{Number of samples in $\vec{x}$}
	\label{eq:Am}
\end{equation}

Once A has been obtained the resampling is as simple as:

\begin{equation*}
	\vec{y} = A\vec{x}
\end{equation*}

As can be seen with the simple example given in Equation \ref{eq:Aexample}. When scaling by a factor of 2 (p=2,q=1), an additional element has been added between the two pre-existing values.

\begin{equation}
	\vec{y} =
	\begin{pmatrix}
	1 & 0 \\
	0.5 & 0.5 \\
	0 & 1 \\
	\end{pmatrix}
	\begin{pmatrix}
	\dots \\ 100 \\ 30 \\ \dots
	\end{pmatrix}
	=
	\begin{pmatrix}
	\dots \\ 100 \\ 65 \\ 35 \\ \dots
	\end{pmatrix}
	\label{eq:Aexample}
\end{equation}

\section{Detecting resampling}

Resampling can now be detected by noticing that $65 = 100*0.5+35*0.5$. Obviously a single occurrence of this relationship isn't enough to determine that the image has been resampled. It is therefore necessary to estimate the probability of each sample being related to its neighbouring samples.

%After having We will then be capable of finding the frequency of such an occurrence using a fourier transform to detect the frequency of such events.

\subsection{Probability maps}

\begin{figure}[h]
	\centering
	\begin{subfigure}[b]{0.3\textwidth}
	 	\centering
	    \includegraphics[width=50pt]{output/4_3}
	    \caption[]{\small50x50px 4/3}
	    \label{fig:4_3}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.3\textwidth}
	 	\centering
	    \includegraphics{output/prob_4_3}
	    \caption[]{\small Probability Map}
	    \label{fig:prob}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.3\textwidth}
	 	\centering
	    \includegraphics{output/fft_4_3}
	    \caption[]{\small FFT(P)}
	    \label{fig:fft}
	\end{subfigure}
\end{figure}

To obtain the probability map (The probability of a sample (pixel) being related to its neighbours) as shown in Figure \ref{fig:prob}, Popescu and Farid implemented an Expectation Maximisation algorithm. Where the probability of each sample being correlated with it neighbours $P(i)$ is calculated using Equation \ref{eq:em}.

\begin{equation}\begin{split}
P(i) &=\frac{1}{\sigma \sqrt{2\pi}}\exp{\left(\frac{-R(i)^2}{2\sigma^2}\right)}\\
\\
R(i) &= \abs{y_i-\sum_{k=-N}^{k=N}{\alpha_k y_{i+k}}}
\label{eq:em}
\end{split}\end{equation}

$\sigma$ and $\alpha$ are variables which are said to converge, and are recalculated with each iteration of the algorithm. $\alpha$ is initially set to a random value, while $\sigma$ is assigned a reasonable value ($\approx0.75$).  It is also worth noting that $\alpha_0$ should always equal zero. Which means the above summation shown in Equation \ref{eq:em}, does not need to take into consideration when $k=0$. 
%only needs to take into consideration $N \leq k < 0, 0 < k \leq N$. %You will see are we can make use of this later on by reusing the rows in are $Y$ matrix.
$\sigma$ and $\alpha$ is then updated on each iteration after calculating $P(i)$ using the formulas shown in Equation \ref{eq:alphaupdate}.

\begin{equation}\begin{split}
\vec{\alpha} = (Y^T W Y)^{-1} Y^T W \vec{y}\\
\\
\sigma = \sqrt{\frac{\sum{\vec{w}R^2}}{\sum{\vec{w}}}}
\label{eq:alphaupdate}
\end{split}\end{equation}

Where the rows of Y contain the neighbours of each sample but not the sample itself. As shown in Equation \ref{eq:Ysample} when $N=\pm2$.

\begin{equation}\begin{split}
\vec{y} &= \begin{pmatrix}a & b & c & d & e & f & g\end{pmatrix}\\
\\
Y_c &= \begin{pmatrix}a & b & d & e\end{pmatrix}\\
Y_d &= \begin{pmatrix}b & c & e & f\end{pmatrix}\\
Y_e &= \begin{pmatrix}c & d & f & g\end{pmatrix}\\
\\
Y &=
\begin{pmatrix}
a & b & d & e \\
b & c & e & f \\
c & d & f & g \\
\end{pmatrix}
\label{eq:Ysample}
\end{split}\end{equation}

And W is the diagonal weighting matrix formed from the weighting vector (which is also used to calculate $\sigma$) $\vec{w}$.

\begin{equation*}
\vec{w} = \frac{P(i)}{P(i)+p_0}\qquad\qquad p_0=\frac{1}{\text{Range of $\vec{y}$}}
\end{equation*}

You'll notice that $Y_a$, $Y_b$, $Y_f$, $Y_g$ is left out of $Y$ as they have $<N$ neighbours, It is not possible to calculate the probability for these values and are thus excluded from the probability maps and can safely be remove from $\vec{y}$ for the remainder of the algorithm steps.

The algorithm continues recalculating $P(i)$ values for each sample and updating the $\alpha$,$\sigma$ values to be used in the next iteration. Until the difference between the current alpha value and the next calculated alpha value is below a certain threshold. At which point the final alpha and probability values are returned.

\subsubsection{Application to a 2D lattice}
Applying the above algorithms to a 2D lattice such as an image requires minimal changes. In the case of resampling it simply involves running the same algorithm twice to resample it in one direction, before transposing and resampling it in the other direction. The final result then needs to be transposed back.

In the case of EM, the neighbourhood is taken both in the vertical and horizontal. The boundary elements are removed from all sides of the image. After the neighbourhood has been built and boundary elements removed, the remaining matrix is then vectorised. As shown in Equation \ref{eq:2DY} with $N=\pm1$.

\begin{equation}\begin{split}
y &= \begin{pmatrix}
a & b & c & d\\
e & f & g & h\\
i & j & k & l\\
m & n & o & p\\
\end{pmatrix}\\
\\
Y_f &= \begin{pmatrix}a & b & c & e & g & i & j & k\end{pmatrix}\\
\\
\vec{y} &= \begin{pmatrix} a & b & c & d & e & f & g \dots \end{pmatrix}\\
\label{eq:2DY}
\end{split}\end{equation}

\subsection{Evaluating probability maps using synthetic maps}

\begin{figure}[h]
 	\centering
    \includegraphics{output/syn_4_3}
    \caption[]{\small Synthetic Map 50x50px 4/3 sampling rate}
    \label{fig:syn}
\end{figure}

The possibility map on its own is of little value, instead a fourier transform is applied to it, which highlights areas of increased frequency such as many high probabilities. Using this information it is possible to visually inspect the transform and decide if it has been resampled. As shown by the peaks in Figure \ref{fig:fft}.

However Popescu and Farid further discussed an algorithm to return a boolean value as to whether the image had been resampled. It involves the generation of synthetic maps, which act as an ideal probability map. A synthetic map must be generated for each resampling rate that it is to check for. However it is crucial to understand that the process does not match an image with a specific resampling rate, but if it is similar to any synthetic map then all that can be deduced is that the image has been resampled. This is caused by how different resampling rates can create the same pattern within the same area, but may differ beyond that.

Synthetic maps are generated by applying a transformation to each (pixel/vertex) index of the sample. The point that is generated is likely to be non-integer. The magnitude at that point is then set to the distance between the transformed point and the closest integer index. As shown in Figure \ref{fig:lattice}.

\begin{eqnarray*}
x &={1..5} \qquad y={1..5} \qquad i = \norm{p-r} \\
\\
\begin{pmatrix}
x_p\\
y_p\\
\end{pmatrix}
&=
\begin{pmatrix}
qx/px & 0 \\
0 & qy/qx\\
\end{pmatrix}
\begin{pmatrix}
x_i\\
y_i\\
\end{pmatrix}
\\
\end{eqnarray*}

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{output/lattice}
\caption{Integer lattice of size 5x5, showing the input point, the transformed point and the closest integer point to it.}
\label{fig:lattice}
\end{figure}

\subsection{Detection of resampling using similarity values}
The final step in detecting if an image has been resampled involves comparing the FFT of the probability map against the FFT of each of the synthetic maps, a similarity value is returned for each comparison. The maximum value of all the comparisons made against a specific probability map is then found and checked against a pre-specified threshold value. If it is above the threshold the image has been resampled. The threshold is set through experimentation. One way of obtaining this value is by passing images known not to have been resampled and taking the maximum similarity value.

Before the maps can be compared the probability map must undergo filtering and gamma correction. The probability map is first filtered before the Fourier transform and after, it is then gamma corrected. As shown in Equation \ref{eq:filter}. Figure \ref{fig:filtersHW} provides a graphical representation of each of the filters presented.

\begin{equation}\begin{split}
&FFT_p = fft(PW)H\\
\\
&GFTT_p = \frac{FFT_p}{max(abs(FFT_p))}^4 \times max(abs(FFT_p));\\
\\
\\
&H(r) = \frac{1}{2} - \frac{1}{2}\cos{\frac{\pi r}{\sqrt(2)}} \qquad 0 \leq r \leq \sqrt(2)\\
\\
&W(r) =
\begin{cases}
1 & 0 \leq r \leq \frac{3}{4} \\ 
\frac{1}{2}+\frac{1}{2}\cos{\frac{\pi(r-\frac{3}{4})} {\sqrt{2}-\frac{3}{4}}}   & \frac{3}{4} \leq r \leq \sqrt{2}
\end{cases}
\label{eq:filter}
\end{split}\end{equation}

\begin{figure}[h]
	\begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{output/H}
        \caption[]{{\small H}}
        \label{fig:H}
    \end{subfigure}
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{output/W}
        \caption[]{{\small W}}
        \label{fig:W}
    \end{subfigure}
    \caption{}
    \label{fig:filtersHW}
\end{figure}

The similarity value is then calculated using Equation \ref{eq:simval}.

\begin{equation}
M = \sum{\abs{GFFT_p}\abs{FFT_s}}
\label{eq:simval}
\end{equation}

\section{Results}
The experiment was run with a small sample size of 20 50x50px images cropped from 2 TIFF images taken from a COOLPIX 950. The small sample size is in part due to to the time taken for the EM algorithm to complete. Each image was then upsampled by 6/5, 5/4, 4/3, 7/5, 3/2, 8/5, 5/3, 7/4, 9/5, and 2/1, and were then processed to determine if the algorithm correctly detected that the image had been resampled. The similarity measure threshold was calculated by placing the original unsampled cropped images through the same process and setting the threshold to the largest similarity measure from that set. The distribution of values and the threshold can be seen in Figure \ref{fig:results}. There was a 100\% success rate on detecting resampling across the board of tested values.

\begin{figure}[h]
	\begin{subfigure}[b]{\textwidth}
	 	\centering
        \includegraphics[width=\textwidth]{output/distribution}
        \caption[]{{\small Similarity measure (y) against resampling rate (x)}}
        \label{fig:distribution}
    \end{subfigure}
    \begin{subfigure}[b]{\textwidth}
        \centering
        \includegraphics[width=\textwidth]{output/success_rate}
        \caption[]{{\small Success rate (y\%) on detecting resampling at various rates (x)}}
        \label{fig:success_rate}
    \end{subfigure}
    \caption{}
    \label{fig:results}
\end{figure}

As can be seen in Figure \ref{fig:results}, the distribution of each sampling rate is dense and well above the minimum threshold for detection.

The next step would be to examine downsampling and rotation. While the current implementation supports downsampling there was limited time to collect the results. Rotation detection could be supported by adding relevant transformation matrices used to generate the synthetic maps. A binomial filter could also be added to the EM algorithm in order to improve the performance of the system and allow a greater sample size, as suggested by Popescu and Farid.

\subsection{Attacks}
As the technique involves the detection of resampling, and often necessitates the original image file taken from the camera in order to prove there has been no forgery. Any attack made on the image is likely to cause resampling which in the eyes of this technique would consider it as a forged image file. Therefore it is not relevant to remark on its ability to withstand attacks. However as mentioned in Popescu and Farid paper, the technique does work for occasions in which the image has been converted to another file format such as JPEG, which is a commonly used photograph format. While the artefacts inherent in JPEG would often trigger false positives, it was able to perform correctly when the JPEG image was of sufficiently high quality. Due to time constraints it was not possible to obtain experimental results using the provided implementation.

\section{Observations}
During implementation various observations were made. One such observation was when the sample data had zero correlation between each other, the weighting matrix $W$ would contain an all-zero row and column. This meant it was not possible to obtain an accurate value for its inverse. The resolution was that this was only ever encountered when using test data as opposed to an actual photograph. If the problem was to occur in real data, it would be reasonable to set the element to something not equalling zero, as the algorithm would be allowed to continue its execution and continue to correctly converge to the destined value.

As Popescu and Farid testing technique involved cropping the image first and then resampling, it is likely to cause a bias within the system, as the similarity measure involves the summation of pixels. Cropping first and later resampling would ensure that there are always more pixels for larger resampling rates and crucially that there are more pixels than when the original threshold was calculated. Although this may necessitate in choosing a better threshold value. It can also be observed in Figure \ref{fig:distribution} were each sample rate has an increase in similarity over the previous rate. This requires further investigation as it may be causing a skewing in the results.

\end{document}


