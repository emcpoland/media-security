mkdir('output/original');

w = 50, h = 50 % Image size to crop to
s = 1; e = 2; % Which images to load 1-2

% List each sampling rate which the images most be resampled too (Keep in sync with main)
pq = [6 5; 5 4; 4 3; 7 5; 3 2; 8 5; 5 3; 7 4; 9 5; 2 1;]%1 5; 1 4; 1 3; 2 5; 1 2; 3 5; 2 3; 3 4; 4 5; 1 1;

numberOfChecks = length(pq);

for i=1:numberOfChecks
    p = pq(i,1); q = pq(i,2);
    mkdir(sprintf('./output/%d_%d/resampled', p, q));
    
    for imageNumber = s:e
        % Load each image in Greyscale
        img = rgb2gray(imread(sprintf('./input/TIFF/%d.tiff', imageNumber)));

        % Scan image, cropping it into subimages
        subImageNumber = 1;
        for x=1:w:size(img,2)-w-1
            for y=1:h:size(img,1)-h-1
                
                % Crop the image
                original = imcrop(img, [x y w-1 h-1]);
                imwrite(original, sprintf('./output/original/%d.tiff', subImageNumber));

                % Resample the cropped image
                resampled = linearlyResampleImage(original, p,q);
                imwrite(resampled, sprintf('./output/%d_%d/resampled/%d.tiff', p, q, subImageNumber));

                subImageNumber=subImageNumber+1;
            end
        end
    end
end


