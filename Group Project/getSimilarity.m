function [M] = getSimilarity(p, s)
	[mp np] = size(p);
	sqrt2 = sqrt(2); % Save as constant to improve performance

	%%%%%%%%%%%%%%%%%%%%%%%%% Generate W
	W = zeros(mp,np);
	maxR = norm([mp/2;np/2]);
	for x=1:np
		for y =1:mp
			r = sqrt2*(norm([mp/2;np/2]-[x;y])/maxR);
			if r < 3/4
				W(x,y) = 1;
			else
				W(x,y)= 0.5+0.5*cos((pi*(r-3/4))/(sqrt2-3/4));
			end
		end
	end
	%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%% Generate H
	H = zeros(mp,np);
	for x=1:np
		for y =1:mp
			r = sqrt2*(norm([mp/2;np/2]-[x;y])/maxR);
			H(x,y)= 0.5+0.5*cos((pi*(r-3/4))/(sqrt2-3/4));
		end
	end
	%%%%%%%%%%%%%%%%%%%%%%%%%

    % Adjust fft(P) 
	P = fft2(p*W);
	Ph = P*H;
	Pg = ((Ph/max(abs(Ph))).^4)*max(abs(Ph));
    
	S = fft2(s);

    % Calculate similarity between P and S
	M = sum(sum(abs(Pg)*abs(S)));
end
